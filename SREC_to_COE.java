//Converts WRAMP srec files to COE files for loading into FPGA block rams
//Written by Aaron Storey -   06/2014
 
import java.io.*;
 
public class SREC_to_COE {
 
        public static void main(String[] args) {
               
                //create file object
	        File file = new File(args[0]);
                BufferedReader bin = null;
		BufferedWriter bout = null;
		File fileOut = new File(args[0] + ".COE");
               
                try
                {
                        //create FileInputStream object
                        FileReader fin = new FileReader(file);
			FileWriter fout = new FileWriter(fileOut);
                       
                        //create object of BufferedInputStream
                        bin = new BufferedReader(fin);
			bout = new BufferedWriter(fout);
			

			//create header for file
			System.out.println("memory_initialization_radix=2;");
			System.out.println("memory_initialization_vector=");
			bout.write("memory_initialization_radix=2;\n");
			bout.write("memory_initialization_vector=\n");
			boolean doComma = false;


		
                         //read file using BufferedInputStream
                        while( bin.ready()){
                               
			    String line = bin.readLine();
			    if (line.startsWith("S3"))
				{			
				    int length = line.length();
				    line = line.substring(12, length -2);
				    int i = 0;
				    int index = 0;
				    char[] charArray = line.toCharArray();
				    while(index < line.length())
					{	    
					    if(doComma)
						{
						    System.out.println(",");
						    bout.write(",\n");
						}
					    else
						doComma = true;

					    for (i = 0;i < 8;i++)
						{
						    String outLine = hex_to_binary(charArray[index]);
						    System.out.print(outLine);
						    bout.write(outLine);
						    index++;
						}
					}
				}
                        }
			System.out.println(";");
			bout.write(";\n");
                       
                }
                catch(FileNotFoundException e)
                {
                        System.out.println("File not found" + e);
                }
                catch(IOException ioe)
                {
                        System.out.println("Exception while reading the file " + ioe); 
                }
                finally
                {
                        //close the BufferedInputStream using close method
                        try{
                                if(bin != null)
                                        bin.close();
				if(bout != null)
				    bout.close();
                                       
                        }catch(IOException ioe)
                        {
                                System.out.println("Error while closing the stream : " + ioe);
                        }
 
                       
                }
        }


    public static String hex_to_binary(char hex)
    {
	String tmp = "";
	switch(hex)
	    {
	    case '0' : tmp = "0000";
		break;
	    case '1' : tmp = "0001";
		break;
	    case '2' : tmp = "0010";
		break;
	    case '3' : tmp = "0011";
		break;
	    case '4' : tmp = "0100";
		break;
	    case '5' : tmp = "0101";
		break;
	    case '6' : tmp = "0110";
		break;
	    case '7' : tmp = "0111";
		break;
	    case '8' : tmp = "1000";
		break;
	    case '9' : tmp = "1001";
		break;
	    case 'A' : tmp = "1010";
		break;
	    case 'B' : tmp = "1011";
		break;
	    case 'C' : tmp = "1100";
		break;
	    case 'D' : tmp = "1101";
		break;
	    case 'E' : tmp = "1110";
		break;
	    case 'F' : tmp = "1111";
		break;
	    case 'a' : tmp = "1010";
		break;
	    case 'b' : tmp = "1011";
		break;
	    case 'c' : tmp = "1100";
		break;
	    case 'd' : tmp = "1101";
		break;
	    case 'e' : tmp = "1110";
		break;
	    case 'f' : tmp = "1111";
		break;
	    default : tmp = "0000";
		break;

	    }
	return tmp;
    }
       
}